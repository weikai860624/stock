# Generated by Django 3.2.7 on 2021-12-08 09:16

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('v1', '0004_auto_20211208_1601'),
    ]

    operations = [
        migrations.AlterField(
            model_name='classifytype',
            name='id',
            field=models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID'),
        ),
    ]
