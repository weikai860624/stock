from django.urls import path, include
from rest_framework.routers import DefaultRouter
from .api import classifytype
from .api import company
from .api import data
from .api import test

urlpatterns = [
    path('v1/',include([
        # path('company/', include(company) ),
        # path('data/', include(data) ),
        # path('classifytype',include(classifytype)),
        #因swagger 才需要用Serializer 
        path('test/', test.test )
    ]))
]

router = DefaultRouter()
router.register(r'classifytype', classifytype.ClassifyTypeAPI, basename='classifytype')
router.register(r'company', company.CompanyAPI, basename='company')
router.register(r'data', data.DataAPI, basename='data')
urlpatterns = router.urls