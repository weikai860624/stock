from django.http import JsonResponse
from django.db import transaction
from rest_framework.generics import GenericAPIView
from rest_framework import viewsets
from rest_framework.permissions import AllowAny, IsAdminUser
from drf_yasg.utils import swagger_auto_schema
from v1.models import ObjectData
from v1.serializer import ObjectDataSerializer
# from rest_framework.routers import DefaultRouter
# from rest_framework.decorators import action

class DataAPI(viewsets.ModelViewSet):
    queryset = ObjectData.objects.all()
    serializer_class = ObjectDataSerializer
    permission_classes_by_action = {    
                                'create': [IsAdminUser],
                                'list': [IsAdminUser],
                                'retrieve':[AllowAny],
                                'update':[AllowAny],
                                'partial_update':[AllowAny],
                                'destroy':[AllowAny]
                                }

# router = DefaultRouter()
# router.register(r'', DataAPI, basename='data')
# urlpatterns = router.urls

# class DataAPI(GenericAPIView):

#     @swagger_auto_schema(
#         operation_summary='我是 GET 的摘要',
#         operation_description='我是 GET 的說明',
#     )
#     def get(self,request):
#         return JsonResponse({'OK':'OK23'})
#         pass
    
#     @swagger_auto_schema(
#         operation_summary='我是 POST 的摘要',
#         operation_description='我是 POST 的說明',
#     )
#     def post(self,request):
#         pass

#     @swagger_auto_schema(
#         operation_summary='我是 PUT 的摘要',
#         operation_description='我是 PUT 的說明',
#     )
#     def put(self,request):
#         pass
    
#     @swagger_auto_schema(
#         operation_summary='我是 DELETE 的摘要',
#         operation_description='我是 DELTET 的說明',
#     )
#     def delete(self,request):
#         pass