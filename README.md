# Stock
為了股票所設計的後端 , 經由此控制API,有控制ＤＢ,前端資訊,後端資料處理 

目前資料先使用此 [finmind API 取得 ](option\tools\external\stock_finmind.py)
說明:
https://blog.jiatool.com/posts/stock_finmind/
https://finmind.github.io/tutor/TaiwanMarket/Derivative/
https://github.com/FinMind/FinMind
https://finmindtrade.com/



並使用 Pyecharts 進行繪圖
## Install

1.  {PROJECT_PATH}/stock/ 
    ```pipenv --python {PYTHON_PATH}```
    
    
2.  {PROJECT_PATH}/stock/$ 
    ```pipenv sync```
    >via Pipfile & Pipfile.lock install library

3.  {PROJECT_PATH}/stock/$ 
    ```pipenv shell```
    >enter virtual environment
 
4.  {PROJECT_PATH}/stock/$ 
    ```python manage.py runserver```
    >execute backend server

5.  add config.json in stock/config.json
```
{
    "SECRET_KEY": "lnv50535!l8ks=0p!=mamr_y)^-3x&91+p-f&f@f63v3k(6c-(",
    "DB_ENGINE" : "django.db.backends.postgresql_psycopg2",
    "DB_NAME":  "op",
    "DB_USERNAME": "postgres",
    "DB_PASSWORD" : "password",
    "DB_HOST" :"127.0.0.1",
    "DB_PORT" :"5432"
}
```

## software development

1. 序列化 
    > https://zoejoyuliao.medium.com/%E7%94%A8-django-rest-framework-%E6%92%B0%E5%AF%AB-restful-api-%E4%B8%A6%E7%94%9F%E6%88%90-swagger-%E6%96%87%E6%AA%94-7cbef7c8e8d6

2. Swagger (因swagger Serializer 才能有效繼承)
    > https://zoejoyuliao.medium.com/%E7%94%A8-django-rest-framework-%E6%92%B0%E5%AF%AB-restful-api-%E4%B8%A6%E7%94%9F%E6%88%90-swagger-%E6%96%87%E6%AA%94-%E4%B8%8B-%E7%94%9F%E6%88%90-swagger-%E6%96%87%E6%AA%94-60c45e04afa8

3. 