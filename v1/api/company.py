from django.http import JsonResponse
from django.db import transaction
from rest_framework.generics import GenericAPIView
from rest_framework import viewsets
from rest_framework.permissions import AllowAny, IsAdminUser
from drf_yasg.utils import swagger_auto_schema
from v1.models import CompanyProfile
from v1.serializer import CompanyProfileSerializer
# from rest_framework.routers import DefaultRouter

'''
在setting設定使用權限    https://www.796t.com/post/N2oxODA=.html
全部method都使用         https://ithelp.ithome.com.tw/articles/10233867
如不想全部method都使用    https://stackoverflow.com/Questions/35970970/django-rest-Framework-permission-Classes-of-viewset-method
                        https://www.django-rest-framework.org/api-guide/viewsets/

'''
class CompanyAPI(viewsets.ModelViewSet):
    queryset = CompanyProfile.objects.all()
    serializer_class = CompanyProfileSerializer
    permission_classes_by_action = {    
                                    'create': [IsAdminUser],
                                    'list': [IsAdminUser],
                                    'retrieve':[AllowAny],
                                    'update':[AllowAny],
                                    'partial_update':[AllowAny],
                                    'destroy':[AllowAny]
                                    }
                                    

# router = DefaultRouter()
# router.register(r'', CompanyAPI, basename='company')
# urlpatterns = router.urls

# class CompanyAPI(GenericAPIView):

#     @swagger_auto_schema(
#         operation_summary='我是 GET 的摘要',
#         operation_description='我是 GET 的說明',
#     )
#     def get(self,request):
        
#         return JsonResponse({'OK':'OK23'})
#         pass
    
#     @swagger_auto_schema(
#         operation_summary='我是 POST 的摘要',
#         operation_description='我是 POST 的說明',
#     )
#     def post(self,request):
#         pass

#     @swagger_auto_schema(
#         operation_summary='我是 PUT 的摘要',
#         operation_description='我是 PUT 的說明',
#     )
#     def put(self,request):
#         pass
    
#     @swagger_auto_schema(
#         operation_summary='我是 DELETE 的摘要',
#         operation_description='我是 DELTET 的說明',
#     )
#     def delete(self,request):
#         pass