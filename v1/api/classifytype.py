from django.http import JsonResponse
from django.db import router, transaction
from rest_framework.generics import GenericAPIView
from rest_framework import viewsets
from rest_framework.permissions import AllowAny, IsAdminUser
from drf_yasg.utils import swagger_auto_schema
from v1.models import ClassifyType
from v1.serializer import ClassifyTypeSerializer
from rest_framework.routers import DefaultRouter

class ClassifyTypeAPI(viewsets.ModelViewSet):
    queryset = ClassifyType.objects.all()
    serializer_class = ClassifyTypeSerializer
    permission_classes_by_action = {    
                                    'create': [IsAdminUser],
                                    'list': [IsAdminUser],
                                    'retrieve':[AllowAny],
                                    'update':[AllowAny],
                                    'partial_update':[AllowAny],
                                    'destroy':[AllowAny]
                                    }


