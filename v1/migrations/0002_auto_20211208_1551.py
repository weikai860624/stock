# Generated by Django 3.2.7 on 2021-12-08 07:51

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('v1', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='ClassifyType',
            fields=[
                ('id', models.IntegerField(primary_key=True, serialize=False)),
                ('name', models.CharField(max_length=1000, unique=True)),
            ],
        ),
        migrations.AddField(
            model_name='companyprofile',
            name='main',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.DO_NOTHING, to='v1.classifytype'),
        ),
    ]
